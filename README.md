Для того чтобы подключить в composer.json проекта прописать:
```composer
"repositories": [
    {
        "type": "package",
        "package": {
            "name": "matchtv/matchtv",
            "version": "1.0",
            "type": "symfony-bundle",
            "source": {
                "url": "https://gitlab.com/ashur.umarov/matchtv.git",
                "type": "git",
                "reference": "master"
            },
            "autoload": {
                "classmap": ["/"]
            }
        }
    }
]
```
После:
```composer
composer require "matchtv/matchtv"
```

Добавить в `bundles.php`:
```php
Match\MatchBundle\MatchBundle::class => ['all' => true],
``` 

Добавить файл `matchtv.yaml` в папку `config/routes` и вписать туда
```yaml
matchtv:
    resource: "@MatchBundle/Controller/MatchController.php"
    type: annotation
```

Добавить в файлы `config/packages/dev/monolog.yaml` и `config/packages/prod/monolog.yaml` в тег `handlers` 
```yaml
doctrine:
    channels: ['doctrine']
    type: service
    id: application_request.handler.doctrine_handler
```