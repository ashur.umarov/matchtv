<?php
namespace Match\MatchBundle\Controller;

use Match\MatchBundle\Entity\Log;
use Match\MatchBundle\Repository\LogRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Template;

class MatchController extends AbstractController
{

    /**
     * @Route("/admin/http-log/find")
     * @return Template
     */
    public function findByClientIp(Request $request)
    {
        $clientIP = $request->get('ip');
        $doctrineManager = $this->getDoctrine()->getManager();
        $repository = $doctrineManager->getRepository('MatchBundle:Log');
        $records = $repository->findByClientIP($clientIP);

        return $this->render(
            '@Match\log.html.twig',
            ['records' => $records]
        );
    }

    /**
     * @Route("/admin/http-log")
     * @return Template
     */
    public function test()
    {
        $doctrineManager = $this->getDoctrine()->getManager();
        $repository = $doctrineManager->getRepository('MatchBundle:Log');

        $records = $repository->getFirstTwentyRecords();

        return $this->render(
            '@Match\log.html.twig',
            ['records' => $records]
        );
    }
}