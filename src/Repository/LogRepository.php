<?php

namespace Match\MatchBundle\Repository;

use Doctrine\ORM\EntityRepository;

class LogRepository extends EntityRepository
{
    public function getFirstTwentyRecords()
    {
        $em = $this->getEntityManager();
        $connection = $em->getConnection();
        $data = $em->createQueryBuilder()
                    ->select("l")
                    ->from('Match\MatchBundle\Entity\Log', 'l')
                    ->orderBy('l.id', 'desc')
                    ->getQuery()
                    ->setFirstResult(0)
                    ->setMaxResults(20)
                    ->getResult();

        return $data;
    }

    public function findByClientIP($ip)
    {
        $em = $this->getEntityManager();
        $connection = $em->getConnection();
        $data = $em->createQueryBuilder()
            ->select("l")
            ->from('Match\MatchBundle\Entity\Log', 'l')
            ->where("l.ip = '$ip'")
            ->orderBy('l.id', 'desc')
            ->getQuery()
            ->setFirstResult(0)
            ->setMaxResults(50)
            ->getResult();

        return $data;
    }
}