<?php
namespace Match\MatchBundle;

use Match\MatchBundle\DependencyInjection\MatchExtension;
use Match\MatchBundle\EventListener\RequestListener;
use Match\MatchBundle\Handler\DoctrineHandler;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class MatchBundle extends Bundle
{

    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
    }
}