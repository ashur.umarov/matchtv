<?php

namespace Match\MatchBundle\EventListener;

use App\Kernel;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ResponseListener
{

    private $logger;

    /**
     * RequestListener constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function onKernelResponse(ResponseEvent $event) {
        $request = $event->getRequest();
        if ($request->query->get('log') === "true") {
            $response            = $event->getResponse();
            $message['channel']  = 'doctrine';
            $message['ip']       = $request->getClientIp();
            $message['request']  = json_encode($request->headers->all()) . '; body: ' . $request->getContent();
            $message['response'] = json_encode($request->headers->all()) . '; body: ' .  $response->getContent();
            $message['status']   = $response->getStatusCode();
            $message['url']      = $request->getHost() . $request->getRequestUri();
            $message['isLog']    = true;
            $this->logger->info(json_encode($message));
        }
    }
}