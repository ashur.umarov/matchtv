<?php

namespace Match\MatchBundle\Handler;

use Match\MatchBundle\Entity\Log;
use Doctrine\ORM\EntityManagerInterface;
use Monolog\Handler\AbstractProcessingHandler;

class DoctrineHandler extends AbstractProcessingHandler
{

    private $entityManager;
    private $channel = 'doctrine';

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
    }

    protected function write(array $record)
    {
        $message = json_decode($record['message']);

        if ($this->channel != $record['channel']) {
            return;
        }

        if($this->entityManager->isOpen() && isset($message->isLog)) {
            $this->entityManager->getConnection()
                ->getConfiguration()
                ->setSQLLogger(null);

            $log = new Log();
            $log->setIp($message->ip);
            $log->setRequest($message->request);
            $log->setResponse($message->response);
            $log->setStatus($message->status);
            $log->setUrl($message->url);
            $this->entityManager->persist($log);
            $this->entityManager->flush();
        }
    }
}